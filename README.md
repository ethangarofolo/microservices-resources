# Microservices Resources

## Code

| | Description |
| -- | -- |
| [The Eventide Project](https://eventide-project.org/) | The best toolkit I've seen for writing microservices.  Ruby-based.|
| [Gearshaft](https://github.com/mpareja/gearshaft) | JavaScript port of Eventide. |
| [MessageDB](https://github.com/message-db/message-db) | PostgreSQL-based message store |

## Read

| | Description |
| -- | -- |
| [Practical Microservices](https://pragprog.com/titles/egmicro/practical-microservices/) |  Somewhat biased on this one---it's my book.  A walkthrough of why to use microservices, building a simple library to interact with a message store, and then building a simple YouTube-like clone. Shows all the pieces that go into such a system. |
| [Event Modeling](https://eventmodeling.org/posts/what-is-event-modeling/) | A useful tool and pattern language for expressing event-based designs.

## Watch

| Title | Speaker(s) | When/Where | Duration | Description |
| -- | -- | -- | -- | -- |
| [Evented Autonomous Services in Ruby](https://www.youtube.com/watch?v=qgKlu5gFsJM) | Scott Bellware | GORUCO 2018 | 30:16 | Talking about the principles behind autonomous services and how they differ from common understandings of "microservices."
| [The Aggregate, The Cohesion, And The Cargo Cult](https://www.youtube.com/watch?v=sb-WO-KcODE) | Scott Bellware and Nathan Ladd | Explore DDD Denver 2017 | 49:31 | The issues with the aggregate pattern from Domain-Driven Design
| [CQRS and Event Sourcing](https://www.youtube.com/watch?v=JHGkaShoyNs) | Greg Young | Code on the Beach 2014 | 1:03:48 | One of the best "intro to event sourcing" talks I've come across
| [Polyglot Data](https://www.youtube.com/watch?v=hv2dKtPq0ME) | Greg Young | GOTO 2014 | 43:29 | Wrong data models cause massive amounts of accidental complexity
| [YOW! Nights October 2015](https://www.youtube.com/watch?v=MTArpO7rSQE) | Udi Dahan | YOW! Nights | 1:23:08 | A great talk where he talks about various aspects of services and service design.  He specifically gets into finding services boundaries at [16:29](https://youtu.be/MTArpO7rSQE?t=989).
| [Why You Had a Bad Time With Microservices](https://www.youtube.com/watch?v=BXRgZpxjqJ0) | Ethan Garofolo | EmpireJS 2018 | 22:41 | Common anti-patterns with microservices
| [Microservices, Monoliths, and Event Stores](https://www.youtube.com/watch?v=ELTZkbHJ-Xg) | Ethan Garofolo | Utah Node.js Meetup 18 Sep 2020 | 1:15:49 | More high-level why with some how
| [Idempotence: Build Mission Critical Systems *and* Keep Your Job](https://www.youtube.com/watch?v=vmPqi8mHHis) | Ethan Garofolo | UtahJS Conf 2019 | 24:43 | Idempotence is a countermeasure against unreliable networks, and this talk gets into it in a pure HTTP context as well as in a message-based services context
| [Choreography vs. Orchestration](https://www.youtube.com/watch?v=ofJd3AZIfto) | Ethan Garofolo | UtahJS Conf 2018 | 37:33 | What do the words mean, and how do they differ in a pub/sub world?
| [Building UIs for Microservices](https://www.youtube.com/watch?v=ArTS_AJ-smQ) | Ethan Garofolo | wroclov.rb 2019 | 38:42 | Considerations for UI in an eventually-consistent system
