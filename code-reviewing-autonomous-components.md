# Code Reviewing Autonomous Components

Whether in an ensemble setting or in an after-the-fact code review, here is a non-exhaustive list of what to look for when reviewing autonomous component code:

1. Is the handler code idempotent?
2. Is the handler code [concurrency-safe](http://docs.eventide-project.org/user-guide/writers/expected-version.html)?
